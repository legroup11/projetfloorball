Feature: Connexion à application Jpetstore

  Scenario: Connextion
    Given un navigateur est ouvert
    When je suis sur url
    And je clique sur le lien de connexion
    And rentre le username "j2ee"
    And rentre le password "j2ee"
    And je clique sur login
    Then utilisateur ABC est connecte
    And je peux lire le message accueil "Welcome ABC !"