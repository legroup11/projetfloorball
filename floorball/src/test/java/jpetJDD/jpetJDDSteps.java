package jpetJDD;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetJDDSteps {
	
	WebDriver driver;
	
	@Given("un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	    driver.manage().window().maximize();
	}

	@When("je suis sur url")
	public void je_suis_sur_url() {
		assertEquals("JPetStore Demo", driver.getTitle());
	}

	@When("je clique sur le lien de connexion")
	public void je_clique_sur_le_lien_de_connexion() {
		driver.findElement(By.id("Menu")).click();
	}

	@When("rentre le username {string}")
	public void rentre_le_username(String username) {
		driver.findElement(By.name("username")).sendKeys(username);
	}

	@When("rentre le password {string}")
	public void rentre_le_password(String password) {
	    driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(password);
	}

	@When("je clique sur login")
	public void je_clique_sur_login() {
		driver.findElement(By.name("signon")).click();
	}

	@Then("utilisateur ABC est connecte")
	public void utilisateur_ABC_est_connecte() {
		assertTrue(driver.findElement(By.xpath("//div[@id='WelcomeContent']")).isDisplayed());
	}

	@Then("je peux lire le message accueil {string}")
	public void je_peux_lire_le_message_accueil(String returnMessage) {
		 assertEquals(returnMessage, driver.findElement(By.xpath("//div[@id='WelcomeContent']")).getText());
	}
}
