Feature: Connexion à application Jpetstore

  Scenario Outline: Connexion à application Jpetstore
    Given un navigateur est ouvert
    When je suis sur url
    And je clique sur le lien de connexion
    And rentre le username <login>
    And rentre le password <password>
    And je clique sur login
    Then utilisateur ABC est connecte
    And je peux lire le message accueil <returnMessage>

  @AdminProfil
    Examples: 
      | login  | password | returnMessage  	|
      | "j2ee" | "j2ee" 	| "Welcome ABC!" |

  @UserProfil
    Examples: 
      | login  | password | returnMessage  	|
      | "ACID" | "ACID" 	| "Welcome ABC!" |